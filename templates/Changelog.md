---
title: {{AppName}} Changelog
description: Changelog for {{AppName}}
hide_table_of_contents: true
---

# Changelog for {{AppName}}

This page contains the changelog for [{{AppName}}](/work/app/{{Name}})

{%- for Release in Releases %}
## {{Release.version}}
{{Release.description.to_html()}}
{%- endfor -%}
