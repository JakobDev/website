---
title: {{Package}}
description: {{Summary}}
hide_table_of_contents: true
---

import CodeBlock from '@theme/CodeBlock';

{{Description}}

## Installation
You can install {{Package}} from [PyPI](https://pypi.org/project/{{Package}}) using pip:
<CodeBlock>
pip install {{Package}}
</CodeBlock>

## Links
{%- for Key, Value in Links.items() %}
[{{Key}}]({{Value}}){{"  "}}
{%- endfor -%}