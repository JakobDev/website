---
title: {{DisplayName}}
description: {{Summary}}
hide_table_of_contents: true
---

<p align="center">
    <img alt="Icon" src="{{IconPath}}" {% raw %}style={{width: "100px", height: "100px"}}{% endraw %}/>
</p>

<h1 align="center">{{DisplayName}}</h1>

<h3 align="center">{{Summary}}</h3>

<p align="center">
    <img src="/img/work/screenshots/webextension/{{Name}}/Screenshot.png"/>
</p>

{{Description}}

[Install for Firefox](https://addons.mozilla.org/de/firefox/addon/{{Firefox}})
