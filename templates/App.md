---
title: {{AppName}}
description: {{Summary}}
hide_table_of_contents: true
---

import CodeBlock from '@theme/CodeBlock';
import TabItem from '@theme/TabItem';
import Tabs from '@theme/Tabs';

<p align="center">
    <img alt="Icon" src="{{IconPath}}" {% raw %}style={{width: "100px", height: "100px"}}{% endraw %}/>
</p>

<h1 align="center">{{AppName}}</h1>

<h3 align="center">{{Summary}}</h3>

<p align="center">
    <img alt="{{ScreenshotCaption}}" src="/img/work/screenshots/app/{{Name}}/{{ScreenshotName}}"/>
</p>

{{Description}}

## Installation

<Tabs>
    {%- if SourceForgeWindows or Winget %}
    <TabItem value="Windows" label="Windows">
        {%- if SourceForgeWindows %}
        <div className="download-tab-margin">
            <h3>Binaries</h3>
            You can find Windows binaries on <a href="https://sourceforge.net/projects/{{SourceForgeWindows}}/files">SourceForge</a>
        </div>
        {%- endif %}
        {%- if Winget %}
        <div>
            <h3>Winget</h3>
            You can install {{AppName}} with Winget:
            <CodeBlock>
            winget install {{Winget}}
            </CodeBlock>
        </div>
        {%- endif %}
    </TabItem>
    {%- endif %}
    {%- if Flatpak or AUR or SourceForgeAppImage %}
    <TabItem value="Linux" label="Linux">
        {%- if Flatpak %}
        <div className="download-tab-margin">
            <h3>Flatpak</h3>
            You can install {{AppName}} from <a href="https://flathub.org/apps/{{Flatpak}}">Flathub</a>
        </div>
        {%- endif %}
        {%- if AUR %}
        <div className="download-tab-margin">
            <h3>AUR</h3>
            If you use a Arch based Distro, you can install {{AppName}} from the <a href="https://aur.archlinux.org/packages/{{AUR}}">AUR</a>
        </div>
        {%- endif %}
        {%- if SourceForgeAppImage %}
        <div>
            <h3>AppImage</h3>
            You can find a AppImage on <a href="https://sourceforge.net/projects/{{SourceForgeAppImage}}/files">SourceForge</a>
        </div>
        {%- endif %}
    </TabItem>
    {%- endif %}
</Tabs>

## Links
{%- if "vcs-browser" in URL %}
[Source Code]({{URL["vcs-browser"]}}){{"  "}}
{%- endif %}
{%- if "bugtracker" in URL %}
[Issue Tracker]({{URL["bugtracker"]}}){{"  "}}
{%- endif %}
{%- if "translate" in URL %}
[Translate]({{URL["translate"]}}){{"  "}}
{%- endif %}

[View changelog](/work/app/{{Name}}/changelog)
