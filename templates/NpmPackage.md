---
title: {{Package}}
description: {{Description}}
hide_table_of_contents: true
---

{{Readme}}

[View {{Package}} on NPM](https://www.npmjs.com/package/{{Package}})
