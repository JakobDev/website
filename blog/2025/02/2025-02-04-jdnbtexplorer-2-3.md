---
slug: 2025-02-04-jdnbtexplorer-2-3
title: jdNBTExplorer 2.3 is out
date: 2025-02-04
authors: [jakobdev]
tags: [jdNBTExplorer, Release]
---
Version 2.3 of [jdNBTExplorer](/work/app/jdNBTExplorer) is now out!
If you open a directory, you can now choose which types of files (NBT files, Region files or both) you want to open.
