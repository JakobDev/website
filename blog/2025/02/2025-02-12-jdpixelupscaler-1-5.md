---
slug: 2025-02-12-jdpixelupscaler-1-5.md
title: jdPixelUpscaler 1.5 is out
date: 2025-02-12
authors: [jakobdev]
tags: [jdPixelUpscaler, Release]
---
Version 1.5 of [jdPixelUpscaler](/work/app/jdPixelUpscaler) is now out!
This Version adds a Estonian translation by [jrtcdbrg](https://codeberg.org/jrtcdbrg).
