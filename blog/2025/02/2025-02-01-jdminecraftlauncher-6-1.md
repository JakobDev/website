---
slug: 2025-02-01-jdminecraftlauncher-6-1
title: jdMinecraftLauncher 6.1 is out
date: 2025-02-01
authors: [jakobdev]
tags: [jdMinecraftLauncher, Release]
---
Version 6.1 of [jdMinecraftLauncher](/work/app/jdMinecraftLauncher) is now out!
The News are now rendered in the same style as the old launcher.
The option to run Minecraft in a subsandbox when running as Flatpak is now enabled by default.
