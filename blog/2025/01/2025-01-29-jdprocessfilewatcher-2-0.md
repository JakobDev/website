---
slug: 2025-01-29-jdprocessfilewatcher-2-0
title: jdProcessFileWatcher 2.0 is out
date: 2025-01-29
authors: [jakobdev]
tags: [jdProcessFileWatcher, Release]
---
Version 4.3 of [jdProcessFileWatcher](/work/app/jdProcessFileWatcher) is now out! This Version improves the performance.
It also adds a dialog in which you can select a process to attach.
Furthermore a Russian translation by [0ko ](https://codeberg.org/0ko) was added.
