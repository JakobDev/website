---
slug: 2024-03-08-jdminecraftlauncher-6-0
title: jdMinecraftLauncher 6.0 is out
date: 2024-03-08
authors: [jakobdev]
tags: [jdMinecraftLauncher, Release]
---

Version 6.0 of [jdMinecraftLauncher](/work/app/jdMinecraftLauncher) has been released! This update brings many exciting changes and will soon be available on Flathub and SourceForge.

<!-- truncate -->

### Change Minecraft Directory

While it was always possible to alter the game directory (where Minecraft stores its data such as worlds, resource packs, and settings) for a profile, 
now you can also change your Minecraft Directory (the installation directory of Minecraft) from the default `.minecraft` directory to any location of your choice in the Options.
If you've ever wanted to store your Minecraft installation on an external drive, now you can!

### Update

jdMinecraftLauncher now checks for updates on every startup. While it cannot automatically install updates, it will notify users of a new version release and provide a download link. This ensures users can easily stay up-to-date.

This feature is disabled when you install jdMinecraftLauncher from Flathub or the AUR.

### Enhanced Wayland Experience

Linux users running Minecraft in a Wayland Session can now force Minecraft to use Wayland/XWayland. Although Minecraft currently defaults to XWayland, users can now experience native Wayland mode by selecting it in the Options. Older Minecraft versions that do not support Wayland will continue to use XWayland regardless of user choice.

### Subsandbox for Flatpak Users

As you may know, the Flatpak version of jdMinecraftLauncher is sandboxed. This means that jdMinecraftLauncher only has access to the specific parts of your system that it needs to function.That makes a lot of sense, especially in the case of a Minecraft launcher. There is always a risk of downloading and installing a Minecraft mod that may contain a virus. Even if you don't use mods, you could be vulnerable to hacking while playing online due to security vulnerabilities in Minecraft itself. Just recall the log4j exploit, which allowed anyone on a server you played on to hack you by sending a chat message.

Running Minecraft in a sandbox significantly enhances your safety. Even if Minecraft is compromised, the virus remains confined within the sandbox. As long as the sandbox itself is free from security vulnerabilities that are exploited, it cannot harm your system.

The latest version of jdMinecraftLauncher takes this a step further by allowing users to run Minecraft in a Subsandbox using `flatpak-spawn`. This creates a Sandbox within a Sandbox, enhancing overall security.

In a Subsandbox, Minecraft only has access to these directories:
- When using a custom game directory:
    - Full access to your game directory
    - Read-only access to your Minecraft directory
    - Full access to `Minecraft directory/versions/<current version>`
- When not using a custom game directory:
    - Full access to your Minecraft directory.

If a custom Java executable is used, Minecraft also has read-only access to the directory of your Java version.

This feature is currently not enabled by default. Users can enable it in the Options to test its functionality.

### Code Refactoring

The new version includes significant code refactoring, aiming to enhance the stability of jdMinecraftLauncher and make future modifications easier. If you encounter any unexpected issues, please [open a bug report](https://codeberg.org/JakobDev/jdMinecraftLauncher/issues).

### Credits

A special acknowledgment goes to the amazing individuals who contributed to the translation of jdMinecraftLauncher into different languages:
- [Heimen Stoffels](https://codeberg.org/Vistaus) (Dutch)
- [AviDevs](https://codeberg.org/AviDevs) (Spanish)
- [gallegonovato](https://codeberg.org/gallegonovato) (Spanish)
- [0ko](https://codeberg.org/0ko) (Russian)

[You can help too!](https://translate.codeberg.org/projects/jdMinecraftLauncher)

