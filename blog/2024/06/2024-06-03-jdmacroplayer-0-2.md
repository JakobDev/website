---
slug: 2024-06-03-jdmacroplayer-0-2
title: jdMacroPlayer 0.2 is out
date: 2024-06-03
authors: [jakobdev]
tags: [jdMacroPlayer, Release]
---
Version 0.2 of [jdMacroPlayer](/work/app/jdMacroPlayer) is now out!
This Version includes many additions and quality of life changes.

<!-- truncate -->

You can now choose if a Macro should be executed when a Shortcut is pressed, while a Shortcut is hold or while a Shortcut is released.

2 new Actions are now included: Sleep and Notification.
Sleep let's you make a pause before the next Action is executed.
Notification let's you show a desktop notification.

The daemon can now run without root access if `/dev/uinput` is writable.
You can set up a custom udev rule to make `/dev/uinput` writable for regular users using the `Daemon>Install udev` rule.
Please be cautious as this allows any program to create a virtual input device.

It's now possible to copy/paste Actions.

There is also now an [Documentation](https://jdmacroplayer.readthedocs.io) that is being worked on.

This new release introduces the following translations (some may not be 100% complete yet):
- Dutch by [Visatus](https://codeberg.org/Vistaus)
- Polish by [Eryk Michalak](https://codeberg.org/ewm)
- Russian by [0ko](https://codeberg.org/0ko)
- Danish by [siken](https://codeberg.org/siken)

You can [contribute to translating jdMacroPlayer](https://translate.codeberg.org/projects/jdMacroPlayer/)

Please keep in mind that jdMacroPlayer is still in Beta.
