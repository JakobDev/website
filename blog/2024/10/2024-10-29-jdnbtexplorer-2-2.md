---
slug: 2024-10-29-jdnbtexplorer-2-2
title: jdNBTExplorer 2.2 is out
date: 2024-10-29:00:02
authors: [jakobdev]
tags: [jdNBTExplorer, Release]
---
Version 2.2 of [jdNBTExplorer](/work/app/jdNBTExplorer) is now out! This Version [fixes opening directories](https://codeberg.org/JakobDev/jdNBTExplorer/issues/5).
