---
slug: 2024-10-29-jdappstreamedit-9-2
title: jdAppStreamEdit 9.2 is out
date: 2024-10-29:00:01
authors: [jakobdev]
tags: [jdAppStreamEdit, Release]
---
Version 9.2 of [jdAppStreamEdit](/work/app/jdAppStreamEdit) is now out! This Version [fixes a crash](https://codeberg.org/JakobDev/jdAppStreamEdit/issues/39) when opening a file with a older Screenshot format.
