---
slug: 2024-05-17-jddbusdebugger-4-0
title: jdDBusDebugger 4.0 is out
date: 2024-05-17
authors: [jakobdev]
tags: [jdDBusDebugger, Release]
---
Version 4.0 of [jdDBusDebugger](/work/app/jdDBusDebugger) has been released!

This version now displays the process name and the username associated with a service in the list.

Additionally, you can now connect to the bus of a running Flatpak. This feature enables you to view the permissions a Flatpak has and assists you in debugging.

Furthermore, you can now share a bus over the network. jdDBusDebugger enables you to initiate a server that other instances of jdDbusDebugger can connect to. This feature allows you to debug from another PC on the same network.
