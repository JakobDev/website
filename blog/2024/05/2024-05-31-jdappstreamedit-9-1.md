---
slug: 2024-05-31-jdappstreamedit-9-1
title: jdAppStreamEdit 9.1 is out
date: 2024-05-31
authors: [jakobdev]
tags: [jdAppStreamEdit, Release]
---
Version 9.1 of [jdAppStreamEdit](/work/app/jdAppStreamEdit) is now out! Branding colors are now support. It also includes a Russian translation (not 100% finished yet) by [0ko](https://codeberg.org/0ko).
