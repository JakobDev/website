---
slug: 2024-01-18-jddesktopentryedit-1-4
title: jdDesktopEntryEdit 1.4 is out
date: 2024-01-18
authors: [jakobdev]
tags: [jdDesktopEntryEdit, Release]
---
Version 1.4 of [jdDesktopEntryEdit](/work/app/jdDesktopEntryEdit) is now out! This Version adds a few Icons to the menu and some buttons. It also fixes a crash in the translate dialog.
