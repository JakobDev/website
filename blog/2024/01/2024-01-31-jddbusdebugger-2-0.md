---
slug: 2024-01-31-jddbusdebugger-2-0
title: jdDBusDebugger 2.0 is out
date: 2024-01-31
authors: [jakobdev]
tags: [jdDBusDebugger, Release]
---
Version 2.0 of [jdDBusDebugger](/work/app/jdDBusDebugger) is now out! It now has a new Icon and supports more types. You can also emit a signal now. It is also ow translated into Dutch by [Vistaus](https://codeberg.org/Vistaus).
