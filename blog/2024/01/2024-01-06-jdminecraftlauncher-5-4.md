---
slug: 2024-01-06-jdminecraftlauncher-5-4
title: jdMinecraftLauncher 5.4 is out
date: 2024-01-06T00:00:02
authors: [jakobdev]
tags: [jdMinecraftLauncher, Release]
---
Version 5.4 of [jdMinecraftLauncher](/work/app/jdMinecraftLauncher) is now out! This is a hotfix which fixes a crash on startup that was introduced in Version 5.3.
