---
slug: 2024-01-06-jdminecraftlauncher-5-3
title: jdMinecraftLauncher 5.3 is out
date: 2024-01-06T00:00:01
authors: [jakobdev]
tags: [jdMinecraftLauncher, Release]
---
Version 5.3 of [jdMinecraftLauncher](/work/app/jdMinecraftLauncher) is now out! It now shows a error if you log in with a Microsoft Account that does not own Minecraft. It also now shows the installation progress in the window icon, if your System supports that.

This new Version also contains now a Spanish translation contributed by [AviDevs](https://codeberg.org/AviDevs) and a Russian translation contributed by [0que](https://codeberg.org/0que).
