---
slug: 2024-01-29-jdpixelupscaler-1-3
title: jdPixelUpscaler 1.3 is out
date: 2024-01-29
authors: [jakobdev]
tags: [jdPixelUpscaler, Release]
---
Version 1.3 of [jdPixelUpscaler](/work/app/jdPixelUpscaler) is now out! The Program has now a new Icon. It also adds Finish translation from [artnay](https://codeberg.org/artnay).
