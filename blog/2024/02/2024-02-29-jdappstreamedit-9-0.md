---
slug: 2024-02-29-jdappstreamedit-9-0
title: jdAppStreamEdit 9.0 is out
date: 2024-02-29
authors: [jakobdev]
tags: [jdAppStreamEdit, Release]
---
Version 9.0 of [jdAppStreamEdit](/work/app/jdAppStreamEdit) is now out! It mostly includes changes o be compatible to the newest AppStream version such as the new `developer` tag.
It also includes improvements to licenses handling thanks to [Alexander Wilms](https://codeberg.org/Alexander-Wilms).
