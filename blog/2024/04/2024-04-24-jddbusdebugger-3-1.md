---
slug: 2024-04-24-jddbusdebugger-3-1
title: jdDBusDebugger 3.1 is out
date: 2024-04-24
authors: [jakobdev]
tags: [jdDBusDebugger, Release]
---

Version 3.1 of [jdDBusDebugger](/work/app/jdDBusDebugger) has been released! This version  fixes a crash.
