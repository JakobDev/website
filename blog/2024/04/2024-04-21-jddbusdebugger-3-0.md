---
slug: 2024-04-21-jddbusdebugger-3-0
title: jdDBusDebugger 3.0 is out
date: 2024-04-21
authors: [jakobdev]
tags: [jdDBusDebugger, Release]
---

Version 3.0 of [jdDBusDebugger](/work/app/jdDBusDebugger) has been released! This version allows you to monitor the whole D-Bus.
It also supports now object path as type.
