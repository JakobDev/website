---
slug: 2024-04-02-jdmacroplayer-released
title: jdMacroPlayer released
date: 2024-04-02
authors: [jakobdev]
tags: [jdMacroPlayer, Release]
---

My new Program [jdMacroPlayer](/work/app/jdMacroPlayer) was released today. It's a Macro Program for Wayland that is made using [ydotool](https://github.com/ReimuNotMoe/ydotool) and the new Global Shortcuts Portal. You should check it out.

It's already on the AUR and it will be on Flathub in the next days.
