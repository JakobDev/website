---
slug: 2024-04-09-jdmrpackinstaller-1-1
title: jdMrpackInstaller 1.1 is out
date: 2024-04-09
authors: [jakobdev]
tags: [jdMrpackInstaller, Release]
---

Version 1.1 of [jdMrpackInstaller](/work/app/jdMrpackInstaller) has been released! This version adds a Russian translation by [0ko](https://codeberg.org/0ko).
