---
slug: 2023-12-19-jdprocessfilewatcher-1-1
title: jdProcessFileWatcher 1.1 is out
date: 2023-12-19
authors: [jakobdev]
tags: [jdProcessFileWatcher, Release]
---
Version 4.2 of [jdProcessFileWatcher](/work/app/jdProcessFileWatcher) is now out! This version adds the Dutch translation from [Vistaus](https://codeberg.org/Vistaus).
