---
slug: 2023-12-22-jdappstreamedit-8-0
title: jdAppStreamEdit 8.0 is out
date: 2023-12-22
authors: [jakobdev]
tags: [jdAppStreamEdit, Release]
---
Version 8.0 of [jdAppStreamEdit](/work/app/jdAppStreamEdit) is now out! It now supports the new scaling factor feature of AppStream 1.0. It is also now possible to mark a Release as Snapshot.

You can now see, how your AppStream looks like when composed into a catalog. You can also now run `appstremcli compose` for any directory you want directly from the GUI.

Many buttons have now Icons.

[Alexander-Wilms](https://codeberg.org/Alexander-Wilms) has joined as a Contributor and had reworked the Translate Window.
