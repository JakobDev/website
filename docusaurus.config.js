// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

import {themes as prismThemes} from 'prism-react-renderer';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'JakobDev',
  tagline: 'My personal Website',
  favicon: 'img/favicon.png',

  // Set the production url of your site here
  url: 'https://jakobdev.codeberg.page',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',


  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: false,
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://codeberg.org/JakobDev/website/src/branch/main/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
        sitemap: {
          lastmod: "date",
          priority: null,
          changefreq: null,
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      image: 'img/social-card.png',
      navbar: {
        title: 'Home',
        logo: {
          alt: 'Logo',
          src: 'img/logo.avif',
        },
        items: [
          {to: '/work', label: 'My Work', position: 'left'},
          {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://github.com/JakobDev',
            position: 'right',
            fontAwesomeIconOnly: 'fa-brands fa-github',
            title: "GitHub",
          },
          {
            href: 'https://gitlab.com/JakobDev',
            position: 'right',
            fontAwesomeIconOnly: 'fa-brands fa-gitlab',
            title: "GitLab",
          },
          {
            href: 'https://social.anoxinon.de/@JakobDev',
            position: 'right',
            fontAwesomeIconOnly: 'fa-brands fa-mastodon',
            title: 'Mastodon',
            rel: 'me',
          },
          {
            href: 'https://codeberg.org/JakobDev',
            label: 'Codeberg',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        copyright: `Copyright © 2023-${new Date().getFullYear()} JakobDev. Built with Docusaurus. <a href="https://codeberg.org/JakobDev/website">View source code</a>.`,
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
      metadata: [
        { name: 'fediverse:creator', content: '@JakobDev@social.anoxinon.de' }
      ],
    }),

    plugins: [
        [require.resolve("docusaurus-plugin-search-local"),
            {
                hashed: true,
                indexPages: true,
            },
        ]
    ],
};

module.exports = config;
