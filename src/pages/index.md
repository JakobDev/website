---
title: Home
description: My personal Website
hide_table_of_contents: true
---

# Welcome to my personal Website!

I'm a Open Source Developer who lover Linux. I mainly code in Python. You can find a list of my Work [here](/work).

This Website is currently in development.

## Links
- [Codeberg](https://codeberg.org/JakobDev)
- [GitHub](https://github.com/JakobDev)
- [GitLab](https://gitlab.com/JakobDev)
