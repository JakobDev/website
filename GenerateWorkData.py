import mdremotifier.mdremotifier
import appstream_python
from typing import Any
from PIL import Image
import pillow_avif
import requests
import tomllib
import pathlib
import shutil
import jinja2
import os
import io


def download_file(url: str, path: str | os.PathLike) -> None:
    if os.path.exists(path):
        return

    try:
        os.makedirs(os.path.dirname(path))
    except FileExistsError:
        pass

    with requests.get(url) as r:
        with open(path, "wb") as f:
            f.write(r.content)


def download_image(url: str, path: str | os.PathLike) -> None:
    path = os.path.splitext(path)[0] + ".avif"

    if os.path.exists(path):
        return

    try:
        os.makedirs(os.path.dirname(path))
    except FileExistsError:
        pass

    with requests.get(url) as r:
        img = Image.open(io.BytesIO(r.content))
        img.save(path)


def generate_app_page(name: str, app_data: dict[str, str], root_path: pathlib.Path) -> dict[str, str]:
    try:
        os.makedirs(root_path / "src" / "pages" / "work" / "app" / name)
    except FileExistsError:
        pass

    icon_name = name + os.path.splitext(app_data["Icon"])[1]

    if icon_name.endswith(".svg"):
        download_file(app_data["Icon"], root_path / "static" / "img" / "work" / "icons" / "app" / icon_name)
    else:
        download_image(app_data["Icon"], root_path / "static" / "img" / "work" / "icons" / "app" / icon_name)
        icon_name = os.path.splitext(icon_name)[0] + ".avif"

    component = appstream_python.AppstreamComponent.from_bytes(requests.get(app_data["AppStream"]).content)

    screenshot = component.screenshots[0]
    image = screenshot.get_source_image()
    download_image(image.url, root_path / "static" / "img" / "work" / "screenshots" / "app" / name / os.path.basename(image.url))

    template_data = {
        **app_data,
        "Name": name,
        "AppName": component.name.get_default_text(),
        "Summary": component.summary.get_default_text(),
        "IconPath": f"/img/work/icons/app/{icon_name}",
        "ScreenshotCaption": screenshot.caption.get_default_text(),
        "ScreenshotName": os.path.splitext(os.path.basename(image.url))[0] + ".avif",
        "Description": component.description.to_html(),
        "URL": component.urls,
        "Releases": component.releases,
    }

    app_template = jinja2.Template((root_path / "templates" / "App.md").read_text(encoding="utf-8"))
    (root_path / "src" / "pages" / "work" / "app" / name / "index.md").write_text(app_template.render(template_data))

    changelog_template = jinja2.Template((root_path / "templates" / "Changelog.md").read_text(encoding="utf-8"))
    (root_path / "src" / "pages" / "work" / "app" / name / "changelog.md").write_text(changelog_template.render(template_data))

    return template_data


def generate_webextension(name: str, extension_data: dict[str, str], root_path: pathlib.Path) -> dict[str, str]:
    try:
        os.makedirs(root_path / "src" / "pages" / "work" / "webextension")
    except FileExistsError:
        pass

    icon_name = name + os.path.splitext(extension_data["Icon"])[1]

    if icon_name.endswith(".svg"):
        download_file(extension_data["Icon"], root_path / "static" / "img" / "work" / "icons" / "webextension" / icon_name)
    else:
        download_image(extension_data["Icon"], root_path / "static" / "img" / "work" / "icons" / "webextension" / icon_name)
        icon_name = os.path.splitext(icon_name)[0] + ".avif"

    download_image(extension_data["Icon"], root_path / "static" / "img" / "work" / "icons" / "webextension" / icon_name)

    firefox_data = requests.get("https://addons.mozilla.org/api/v5/addons/addon/" + extension_data["Firefox"]).json()

    screenshot = firefox_data["previews"][0]
    download_file(screenshot["image_url"], root_path / "static" / "img" / "work" / "screenshots" / "webextension" / name / "Screenshot.png")

    template_data = {
        **extension_data,
        "Name": name,
        "IconPath": f"/img/work/icons/webextension/{icon_name}",
        "DisplayName": firefox_data["name"]["en-US"],
        "Summary": firefox_data["summary"]["en-US"],
        "Description": firefox_data["description"]["en-US"],
    }

    template = jinja2.Template((root_path / "templates" / "Webextension.md").read_text(encoding="utf-8"))
    (root_path / "src" / "pages" / "work" / "webextension" / f"{name}.md").write_text(template.render(template_data))

    return template_data


def generate_python_package(name: str, package_data: dict[str, str], root_path: pathlib.Path) -> dict[str, str]:
    try:
        os.makedirs(root_path / "src" / "pages" / "work" / "python-package")
    except FileExistsError:
        pass

    pypi_data = requests.get(f"https://pypi.org/pypi/{package_data['Package']}/json").json()

    template_data = {
        **package_data,
        "Name": name,
        "Summary": pypi_data["info"]["summary"],
        "Description": pypi_data["info"]["description"],
        "Links": pypi_data["info"]["project_urls"],
    }

    template = jinja2.Template((root_path / "templates" / "PythonPackage.md").read_text(encoding="utf-8"))
    (root_path / "src" / "pages" / "work" / "python-package" / f"{name}.md").write_text(template.render(template_data))

    return template_data


def generate_npm_package(name: str, package_data: dict[str, str], root_path: pathlib.Path) -> dict[str, str]:
    try:
        os.makedirs(root_path / "src" / "pages" / "work" / "npm-package")
    except FileExistsError:
        pass

    npm_data = requests.get(f"https://registry.npmjs.org/{package_data['Package']}").json()

    template_data = {
        **package_data,
        "Name": name,
        "Description": npm_data["description"],
        "Readme": npm_data["readme"],
    }

    template = jinja2.Template((root_path / "templates" / "NpmPackage.md").read_text(encoding="utf-8"))
    (root_path / "src" / "pages" / "work" / "npm-package" / f"{name}.md").write_text(template.render(template_data))

    return template_data


def generate_go_package(name: str, package_data: dict[str, str], root_path: pathlib.Path) -> dict[str, str]:
    try:
        os.makedirs(root_path / "src" / "pages" / "work" / "go-package")
    except FileExistsError:
        pass

    readme = requests.get(package_data["Readme"]).text

    if "UrlPrefix" in package_data:
        readme = mdremotifier.mdremotifier.Render(md=readme, url_prefix=package_data["UrlPrefix"])

    template_data = {
        **package_data,
        "Name": name,
        "ReadmeContent": readme,
    }

    template = jinja2.Template((root_path / "templates" / "GoPackage.md").read_text(encoding="utf-8"))
    (root_path / "src" / "pages" / "work" / "go-package" / f"{name}.md").write_text(template.render(template_data))

    return template_data


def generate_work(data: dict[str, Any], root_path: pathlib.Path) -> None:
    app_list = []
    for key, value in data["App"].items():
        app_list.append(generate_app_page(key, value, root_path))

    webextension_list = []
    for key, value in data["Webextension"].items():
        webextension_list.append(generate_webextension(key, value, root_path))

    website_list = []
    for key, value in data["Website"].items():
        website_list.append(value)

    python_package_list = []
    for key, value in data["PythonPackage"].items():
        python_package_list.append(generate_python_package(key, value, root_path))

    npm_package_list = []
    for key, value in data["NpmPackage"].items():
        npm_package_list.append(generate_npm_package(key, value, root_path))

    go_package_list = []
    for key, value in data["GoPackage"].items():
        go_package_list.append(generate_go_package(key, value, root_path))

    template_data = {
        "Apps": app_list,
        "Webextensions": webextension_list,
        "Websites": website_list,
        "PythonPackages": python_package_list,
        "NpmPackages": npm_package_list,
        "GoPackages": go_package_list,
    }

    template = jinja2.Template((root_path / "templates" / "Work.mdx").read_text(encoding="utf-8"))
    (root_path / "src" / "pages" / "work" / "index.mdx").write_text(template.render(template_data))


def main() -> None:
    root_path = pathlib.Path(__file__).parent

    with open("data.toml", "rb") as f:
        data = tomllib.load(f)

    #try:
    #    shutil.rmtree(root_path / "src" / "pages" / "apps")
    #except FileNotFoundError:
    #    pass

    generate_work(data, root_path)


if __name__ == "__main__":
    main()
